package com.tupuntodeventa.UI;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws IOException{
        Parent adminParent = FXMLLoader.load(getClass().getResource("admin_fxml.fxml"));
        Scene adminScene = new Scene(adminParent);
        primaryStage.setScene(adminScene);
        primaryStage.setWidth(500);
        primaryStage.setHeight(300);
        primaryStage.setTitle("Admin Scene");
        primaryStage.show();


    }
    public static void main(String[] args) {
        launch(args);
    }
}
