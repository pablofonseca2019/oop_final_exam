package com.tupuntodeventa.BL.Direccion;
import accesoBD.Conector;

import java.sql.*;
import java.util.*;


public class MySQLDireccionDAO implements IDireccion{

    @Override
    public void insertar(String direccionExacta, String canton, String distrito, String provincia, double KMSeparacionRestaurante) {
        String query = "INSERT INTO DIRECCION(DireccionExacta, Canton, Distrito, Provincia, KMSeparacionRestaurante)" +
                " VALUES(" + "'" + direccionExacta + "'" + "," + "'" + canton + "'" + "," + "'" + distrito + "'" + "," + "'" + provincia + "'" + "," + "'" +
                KMSeparacionRestaurante + "'" + ")";
        try{
            Conector.getConector().ejecutarSQL(query);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void modificar(String direccionExacta, String canton, String distrito, String provincia, double KMSeparacionRestaurante) {
        String query = "UPDATE DIRECCION SET " + "DireccionExacta=" + "'" + direccionExacta + "'" + "," + "Canton=" + "'" + canton + "'" + "," +
                "Distrito=" + "'" + distrito + "'" + "," + "Provincia=" + "'" + provincia + "'" + "," + "KMSeparacionRestaurante=" +
                "'" + KMSeparacionRestaurante + "'";
        try{
            Conector.getConector().ejecutarSQL(query);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void eliminar(String direccionExacta, String canton) {
        String query = "DELETE FROM DIRECCION WHERE DireccionExacta=" + "'" + direccionExacta +
                "'" + "AND" + "Canton=" + "'" + canton + "'";
        try{
            Conector.getConector().ejecutarSQL(query);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public ArrayList<Direccion> lista() {
        ArrayList<Direccion> direccion = new ArrayList();
        String sql = "SELECT * FROM DIRECCIONES";
        ResultSet rs;
        try {
            rs =  Conector.getConector().ejecutarSQL(sql, true);

            while (rs.next()) {
                String direccionExacta,canton,distrito,provincia;
                double kmSeparacion;
                direccionExacta = rs.getString("DireccionExacta");
                canton = rs.getString("Canton");
                distrito = rs.getString("Distrito");
                provincia = rs.getString("Provincia");
                kmSeparacion =  rs.getDouble("KMSeparacionRestaurante");




                Direccion tmpDireccion = new Direccion(direccionExacta,canton,distrito,provincia,kmSeparacion);


                direccion.add(tmpDireccion);

            }


        } catch (Exception e) {
            e.printStackTrace();
        }
        return direccion;
    }
}
