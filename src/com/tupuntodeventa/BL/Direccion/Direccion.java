package com.tupuntodeventa.BL.Direccion;

public class Direccion {

    //Attributes
    private String DireccionExacta;
    private String Canton;
    private String Distrito;
    private String Provincia;
    private double KMSeparacionRestaurante;

    public Direccion(String direccionExacta, String canton, String distrito, String provincia, double KMSeparacionRestaurante) {
        DireccionExacta = direccionExacta;
        Canton = canton;
        Distrito = distrito;
        Provincia = provincia;
        this.KMSeparacionRestaurante = KMSeparacionRestaurante;
    }

    public String getDireccionExacta() {
        return DireccionExacta;
    }

    //Getters and Setters
    public void setDireccionExacta(String direccionExacta) {
        DireccionExacta = direccionExacta;
    }

    public String getCanton() {
        return Canton;
    }

    public void setCanton(String canton) {
        Canton = canton;
    }

    public String getDistrito() {
        return Distrito;
    }

    public void setDistrito(String distrito) {
        Distrito = distrito;
    }

    public String getProvincia() {
        return Provincia;
    }

    public void setProvincia(String provincia) {
        Provincia = provincia;
    }

    public double getKMSeparacionRestaurante() {
        return KMSeparacionRestaurante;
    }

    public void setKMSeparacionRestaurante(double KMSeparacionRestaurante) {
        this.KMSeparacionRestaurante = KMSeparacionRestaurante;
    }

    //To String Method

    @Override
    public String toString() {
        return "Direccion{" +
                "DireccionExacta='" + DireccionExacta + '\'' +
                ", Canton='" + Canton + '\'' +
                ", Distrito='" + Distrito + '\'' +
                ", Provincia='" + Provincia + '\'' +
                ", KMSeparacionRestaurante=" + KMSeparacionRestaurante +
                '}';
    }

    //Equals Method

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Direccion)) return false;
        Direccion direccion = (Direccion) o;
        return Double.compare(direccion.KMSeparacionRestaurante, KMSeparacionRestaurante) == 0 &&
                DireccionExacta.equals(direccion.DireccionExacta) &&
                Canton.equals(direccion.Canton) &&
                Distrito.equals(direccion.Distrito) &&
                Provincia.equals(direccion.Provincia);
    }
}
