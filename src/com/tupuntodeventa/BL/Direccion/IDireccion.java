package com.tupuntodeventa.BL.Direccion;

import java.util.ArrayList;

public interface IDireccion {
    public void insertar(String direccionExacta, String canton, String distrito, String provincia, double KMSeparacionRestaurante);
    public void modificar(String direccionExacta, String canton, String distrito, String provincia, double KMSeparacionRestaurante);
    public void eliminar(String direccionExacta, String canton);

    public ArrayList<Direccion> lista();
}
