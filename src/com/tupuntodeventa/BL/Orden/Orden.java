package com.tupuntodeventa.BL.Orden;

import com.tupuntodeventa.BL.Cliente.Cliente;
import com.tupuntodeventa.BL.Cupon.Cupon;
import com.tupuntodeventa.BL.Empleado.Empleado;
import com.tupuntodeventa.BL.Producto.Producto;

import java.util.ArrayList;
import java.util.Date;

public class Orden {

    //Attributes
    private Cliente ClientePerteneciente;
    private Empleado EmpleadoRegistrador;
    private Cupon CuponPerteneciente;
    private ArrayList<Producto> detalleProductos;
    private String NombreCliente;
    private String PrimerApellidoCliente;
    private String SegundoApellidoCliente;
    private Date FechaCompraOrden;
    private Date HoraCompraOrden;
    private char TipoOrden;
    private Empleado FuncionarioOrden;
    private double PrecioTotalOrden;
    private double CostoAdicionalOrden;

    //Constructor
    public Orden(String nombreCliente, String primerApellidoCliente, String segundoApellidoCliente, Date fechaCompraOrden, Date horaCompraOrden, char tipoOrden, Empleado funcionarioOrden, double precioTotalOrden, double costoAdicionalOrden) {
        NombreCliente = nombreCliente;
        PrimerApellidoCliente = primerApellidoCliente;
        SegundoApellidoCliente = segundoApellidoCliente;
        FechaCompraOrden = fechaCompraOrden;
        HoraCompraOrden = horaCompraOrden;
        TipoOrden = tipoOrden;
        FuncionarioOrden = funcionarioOrden;
        PrecioTotalOrden = precioTotalOrden;
        CostoAdicionalOrden = costoAdicionalOrden;
    }

    //Getters And Setters
    public String getNombreCliente() {
        return NombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        NombreCliente = nombreCliente;
    }

    public String getPrimerApellidoCliente() {
        return PrimerApellidoCliente;
    }

    public void setPrimerApellidoCliente(String primerApellidoCliente) {
        PrimerApellidoCliente = primerApellidoCliente;
    }

    public String getSegundoApellidoCliente() {
        return SegundoApellidoCliente;
    }

    public void setSegundoApellidoCliente(String segundoApellidoCliente) {
        SegundoApellidoCliente = segundoApellidoCliente;
    }

    public Date getFechaCompraOrden() {
        return FechaCompraOrden;
    }

    public void setFechaCompraOrden(Date fechaCompraOrden) {
        FechaCompraOrden = fechaCompraOrden;
    }

    public Date getHoraCompraOrden() {
        return HoraCompraOrden;
    }

    public void setHoraCompraOrden(Date horaCompraOrden) {
        HoraCompraOrden = horaCompraOrden;
    }

    public char getTipoOrden() {
        return TipoOrden;
    }

    public void setTipoOrden(char tipoOrden) {
        TipoOrden = tipoOrden;
    }

    public Empleado getFuncionarioOrden() {
        return FuncionarioOrden;
    }

    public void setFuncionarioOrden(Empleado funcionarioOrden) {
        FuncionarioOrden = funcionarioOrden;
    }

    public double getPrecioTotalOrden() {
        return PrecioTotalOrden;
    }

    public void setPrecioTotalOrden(double precioTotalOrden) {
        PrecioTotalOrden = precioTotalOrden;
    }

    //To String Method
    //Equals Method
}
