package com.tupuntodeventa.BL.Puesto;

import java.util.ArrayList;
import java.time.*;

public interface IPuesto {
    public void insertar(String nombrePuesto, double salarioBasePuesto, double bonificacionPuesto, double salarioNetoPuesto,
     LocalDate fechaContratacionPuesto);
    public void modificar(String nombrePuesto, double salarioBasePuesto, double bonificacionPuesto, double salarioNetoPuesto,
                          LocalDate fechaContratacionPuesto);
    public void eliminar(String nombrePuesto, LocalDate fechaContratacionPuesto);

    public ArrayList<Puesto> listar();
}
