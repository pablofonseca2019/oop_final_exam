package com.tupuntodeventa.BL.Puesto;

import java.time.LocalDate;

public class Puesto {

    //Attributes
    private int ID;
    private int IDUser;
    private String nombrePuesto;
    private double salarioBasePuesto;
    private double bonificacionPuesto;
    private double salarioNetoPuesto;
    private LocalDate fechaContratacionPuesto;

    //Constructor
    public Puesto(int ID, int IDUser,String nombrePuesto, double salarioBasePuesto, double bonificacionPuesto, double salarioNetoPuesto, LocalDate fechaContratacionPuesto) {
        this.nombrePuesto = nombrePuesto;
        this.salarioBasePuesto = salarioBasePuesto;
        this.bonificacionPuesto = bonificacionPuesto;
        this.salarioNetoPuesto = salarioNetoPuesto;
        this.fechaContratacionPuesto = fechaContratacionPuesto;
    }

    //Getters and Setters
    public String getNombrePuesto() {
        return nombrePuesto;
    }

    public void setNombrePuesto(String nombrePuesto) {
        this.nombrePuesto = nombrePuesto;
    }

    public double getSalarioBasePuesto() {
        return salarioBasePuesto;
    }

    public void setSalarioBasePuesto(double salarioBasePuesto) {
        this.salarioBasePuesto = salarioBasePuesto;
    }

    public double getBonificacionPuesto() {
        return bonificacionPuesto;
    }

    public void setBonificacionPuesto(double bonificacionPuesto) {
        this.bonificacionPuesto = bonificacionPuesto;
    }

    public double getSalarioNetoPuesto() {
        return salarioNetoPuesto;
    }

    public void setSalarioNetoPuesto(double salarioNetoPuesto) {
        this.salarioNetoPuesto = salarioNetoPuesto;
    }

    public LocalDate getFechaContratacionPuesto() {
        return fechaContratacionPuesto;
    }

    public void setFechaContratacionPuesto(LocalDate fechaContratacionPuesto) {
        this.fechaContratacionPuesto = fechaContratacionPuesto;
    }

    //To String Method


    @Override
    public String toString() {
        return "Puesto{" +
                "ID=" + ID +
                ", IDUser=" + IDUser +
                ", nombrePuesto='" + nombrePuesto + '\'' +
                ", salarioBasePuesto=" + salarioBasePuesto +
                ", bonificacionPuesto=" + bonificacionPuesto +
                ", salarioNetoPuesto=" + salarioNetoPuesto +
                ", fechaContratacionPuesto=" + fechaContratacionPuesto +
                '}';
    }

    //Equals Method
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Puesto)) return false;
        Puesto puesto = (Puesto) o;
        return Double.compare(puesto.salarioBasePuesto, salarioBasePuesto) == 0 &&
                Double.compare(puesto.bonificacionPuesto, bonificacionPuesto) == 0 &&
                Double.compare(puesto.salarioNetoPuesto, salarioNetoPuesto) == 0 &&
                nombrePuesto.equals(puesto.nombrePuesto) &&
                fechaContratacionPuesto.equals(puesto.fechaContratacionPuesto);
    }
}
