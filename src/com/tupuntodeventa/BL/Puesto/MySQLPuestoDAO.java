package com.tupuntodeventa.BL.Puesto;
import accesoBD.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.sql.*;
import java.lang.Exception;

public class MySQLPuestoDAO implements IPuesto{
    @Override
    public void insertar(String nombrePuesto, double salarioBasePuesto, double bonificacionPuesto, double salarioNetoPuesto, LocalDate fechaContratacionPuesto) {
        String query = "INSERT INTO PUESTO(NombrePuesto, SalarioBasePuesto, BonificacionPuesto, SalarioNetoPuesto, FechaContratacionPuesto)" +
                " VALUES(" + "'" + nombrePuesto + "'" + "," + "'" + salarioBasePuesto + "'" + "," + "'" + bonificacionPuesto + "'" + "," + "'" + salarioNetoPuesto + "'" + "," + "'" +
                fechaContratacionPuesto + "'" + ")";
        try{
            Conector.getConector().ejecutarSQL(query);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void modificar(String nombrePuesto, double salarioBasePuesto, double bonificacionPuesto, double salarioNetoPuesto, LocalDate fechaContratacionPuesto) {
        String query = "UPDATE PUESTO SET " + "NombrePuesto=" + "'" + nombrePuesto + "'" + "," + "SalarioBasePuesto=" + "'" + salarioBasePuesto + "'" + "," +
                "BonificacionPuesto=" + "'" + bonificacionPuesto + "'" + "," + "SalarioNetoPuesto=" + "'" + salarioNetoPuesto + "'" + "," + "FechaContratacionPuesto=" +
                "'" + fechaContratacionPuesto + "'";
        try{
            Conector.getConector().ejecutarSQL(query);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void eliminar(String nombrePuesto, LocalDate fechaContratacionPuesto) {
        String query = "DELETE FROM DIRECCION WHERE NombrePuesto=" + "'" + nombrePuesto +
                "'" + "AND" + "FechaContratacionPuesto=" + "'" + fechaContratacionPuesto + "'";
        try{
            Conector.getConector().ejecutarSQL(query);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public ArrayList<Puesto> listar() {
        ArrayList<Puesto> puesto = new ArrayList();
        String sql = "SELECT * FROM PUESTOS";
        ResultSet rs;
        try {
            rs = Conector.getConector().ejecutarSQL(sql,true);

            while (rs.next()) {
                int ID,IDUser;
                String nombrePuesto;
                double salarioBase, bonificacionPuesto, salarioNeto;
                LocalDate fechContratacion;
                ID = rs.getInt("ID");
                IDUser = rs.getInt("IDUser");
                nombrePuesto = rs.getString("NombrePuesto");
                salarioBase = rs.getDouble("SalarioBasePuesto");
                bonificacionPuesto = rs.getDouble("BonificacionPuesto");
                salarioNeto = rs.getDouble("SalarioNetoPuesto");
                fechContratacion =  rs.getDate("FechaContratacionPuesto").toLocalDate();

                Puesto tmpPuesto = new Puesto(ID,IDUser,nombrePuesto,salarioBase,bonificacionPuesto,salarioNeto,fechContratacion);


                puesto.add(tmpPuesto);

            }


        } catch (Exception e) {
            e.printStackTrace();
        }
        return puesto;
    }
}
