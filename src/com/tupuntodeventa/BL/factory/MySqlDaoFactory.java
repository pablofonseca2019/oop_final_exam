package com.tupuntodeventa.BL.factory;

import com.tupuntodeventa.BL.Administrador.MySQLAdministradorDAO;
import com.tupuntodeventa.BL.Cliente.MySQLClienteDAO;
import com.tupuntodeventa.BL.Cupon.ICupon;
import com.tupuntodeventa.BL.Cupon.MySQLCuponDAO;
import com.tupuntodeventa.BL.Direccion.IDireccion;
import com.tupuntodeventa.BL.Direccion.MySQLDireccionDAO;
import com.tupuntodeventa.BL.Empleado.MySQLEmpleadoDAO;
import com.tupuntodeventa.BL.Plato.IPlato;
import com.tupuntodeventa.BL.Plato.MySQLPlatoDAO;
import com.tupuntodeventa.BL.Puesto.IPuesto;
import com.tupuntodeventa.BL.Puesto.MySQLPuestoDAO;
import com.tupuntodeventa.BL.Usuario.IUsuario;
import com.tupuntodeventa.BL.Usuario.MySQLUsuarioDAO;

public class MySqlDaoFactory extends DaoFactory {
    @Override
    public IUsuario getUsuarioDAO() { return new MySQLUsuarioDAO(); }
    @Override
    public IUsuario getCLienteDAO() { return new MySQLClienteDAO(); }
    @Override
    public IUsuario getAdministradorDAO() { return new MySQLAdministradorDAO(); }
    @Override
    public IUsuario getEmpleadoDAO() { return new MySQLEmpleadoDAO(); }
    @Override
    public IDireccion getDireccionDAO() { return new MySQLDireccionDAO(); }
    @Override
    public ICupon getCuponDAO() { return new MySQLCuponDAO(); }
    @Override
    public IPuesto getPuestoDAO() { return new MySQLPuestoDAO(); }
    @Override
    public IPlato getPlatoDAO() { return new MySQLPlatoDAO(); }
}
