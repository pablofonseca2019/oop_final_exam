package com.tupuntodeventa.BL.factory;

import com.tupuntodeventa.BL.Cupon.ICupon;
import com.tupuntodeventa.BL.Direccion.IDireccion;
import com.tupuntodeventa.BL.Plato.IPlato;
import com.tupuntodeventa.BL.Puesto.IPuesto;
import com.tupuntodeventa.BL.Usuario.IUsuario;


public abstract class DaoFactory {
    public static final int MYSQL = 1;

    public static DaoFactory getDaoFactory(int factory){
        switch(factory){
            case MYSQL:
                return new MySqlDaoFactory();
            default:
                return null;
        }
    }
    public abstract IUsuario getUsuarioDAO();
    public abstract IUsuario getCLienteDAO();
    public abstract IUsuario getAdministradorDAO();
    public abstract IUsuario getEmpleadoDAO();
    public abstract IDireccion getDireccionDAO();
    public abstract ICupon getCuponDAO();
    public abstract IPuesto getPuestoDAO();
    public abstract IPlato getPlatoDAO();


}
