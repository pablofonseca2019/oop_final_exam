package com.tupuntodeventa.BL.Cliente;

import com.tupuntodeventa.BL.Direccion.Direccion;
import com.tupuntodeventa.BL.Usuario.Usuario;

import java.time.LocalDate;
import java.util.ArrayList;
import java.time.*;

public class Cliente extends Usuario {

    //Attributes

    //Class Relations
    private ArrayList<Direccion> direccionCliente;

    //Constructor
    public Cliente(String claveUsuario, String correoElectronico, String nombrePilaUsuario, String apellidoUnoUsuario, String apellidoDosUsuario, LocalDate fechaNacimientoUsuario, int edadUsuario, String generoUsuario, String identificacionUsuario, String telefonoUsuario) {
        super(claveUsuario, correoElectronico, nombrePilaUsuario, apellidoUnoUsuario, apellidoDosUsuario, fechaNacimientoUsuario, edadUsuario, generoUsuario, identificacionUsuario, telefonoUsuario);
    }

    //Getters and Setters

    //To String Method

    //Equals Method


}
