package com.tupuntodeventa.BL.Cliente;

import com.tupuntodeventa.BL.Usuario.IUsuario;
import com.tupuntodeventa.BL.Usuario.Usuario;
import accesoBD.*;
import java.time.LocalDate;
import java.util.ArrayList;

public class MySQLClienteDAO implements IUsuario {
    @Override
    public void insertar(String userName,String claveUsuario, String correoElectronico, String nombrePilaUsuario, String apellidoUnoUsuario,
                         String apellidoDosUsuario, LocalDate fechaNacimientoUsuario, int edadUsuario, String generoUsuario, String identificacionUsuario, String telefonoUsuario) {
        String tipoUsuario = "Cliente";
        String query = "INSERT INTO USUARIOS(TipoUsuario,ClaveUsuario, CorreoElectronico, NombrePilaUsuario, ApellidoUnoUsuario, ApellidoDosUsuario, FechaNacimientoUsuario, " +
                "EdadUsuario, GeneroUsuario, identificacionUsuario, TelefonoUsuario)" + " VALUES(" + "'" + tipoUsuario + "'" + "," + "'" + "'" + claveUsuario + "'" + "," + "'" +
                correoElectronico + "'" + "," + "'" + nombrePilaUsuario + "'" + "," + "'" + apellidoUnoUsuario + "'" + "," + "'" +
                apellidoDosUsuario + "'" + "," + "'" + fechaNacimientoUsuario + "'" + "," + "'" + edadUsuario + "'" + "," + "'" + generoUsuario + "'" + "," +
                "'" + identificacionUsuario + "'" + "," + "'" + telefonoUsuario + "'" + ")";
        try{
            Conector.getConector().ejecutarSQL(query);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    @Override
    public void modificar(String claveUsuario, String correoElectronico, String nombrePilaUsuario, String apellidoUnoUsuario, String apellidoDosUsuario, LocalDate fechaNacimientoUsuario, int edadUsuario, String generoUsuario, String identificacionUsuario, String telefonoUsuario) {

    }

    @Override
    public void eliminar(String userName, String correoElectronico) {

    }

    @Override
    public ArrayList<Usuario> listar() {
        return null;
    }
}
