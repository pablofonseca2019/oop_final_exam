package com.tupuntodeventa.BL.Empleado;

import com.tupuntodeventa.BL.Usuario.Usuario;

import java.time.LocalDate;


public class Empleado extends Usuario {


    //Constructor
    public Empleado(int ID,String tipoUsuario, String userName, String claveUsuario, String correoElectronico, String nombrePilaUsuario, String apellidoUnoUsuario, String apellidoDosUsuario, LocalDate fechaNacimientoUsuario, int edadUsuario, String generoUsuario, String identificacionUsuario, String telefonoUsuario) {
        super(ID,tipoUsuario,userName,claveUsuario, correoElectronico, nombrePilaUsuario, apellidoUnoUsuario, apellidoDosUsuario, fechaNacimientoUsuario, edadUsuario, generoUsuario, identificacionUsuario, telefonoUsuario);
    }

    //To String Method

    public String toString() {
        return "Empleado{" +
                ", tipoUsuario='" + tipoUsuario + '\'' +
                ", userName='" + userName + '\'' +
                ", claveUsuario='" + claveUsuario + '\'' +
                ", correoElectronico='" + correoElectronico + '\'' +
                ", nombrePilaUsuario='" + nombrePilaUsuario + '\'' +
                ", apellidoUnoUsuario='" + apellidoUnoUsuario + '\'' +
                ", apellidoDosUsuario='" + apellidoDosUsuario + '\'' +
                ", fechaNacimientoUsuario=" + fechaNacimientoUsuario +
                ", edadUsuario=" + edadUsuario +
                ", generoUsuario='" + generoUsuario + '\'' +
                ", identificacionUsuario='" + identificacionUsuario + '\'' +
                ", telefonoUsuario='" + telefonoUsuario + '\'' +
                '}';
    }


    //Equals Method
}
