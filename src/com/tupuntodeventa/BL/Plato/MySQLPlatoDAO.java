package com.tupuntodeventa.BL.Plato;



import accesoBD.Conector;

import java.sql.ResultSet;
import java.util.ArrayList;

public class MySQLPlatoDAO implements IPlato{

    @Override
    public void insertar(double precioPlato, String descripcionPlato) {
        String query = "INSERT INTO PLATO(PrecioPlato, DescripcionPlato)" +
                " VALUES(" + "'" + precioPlato + "'" + "," + "'" + descripcionPlato + "'" + ")";
        try{
            Conector.getConector().ejecutarSQL(query);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void modificar(double precioPlato, String descripcionPlato) {
        String query = "UPDATE PLATO SET " + "PrecioPlato=" + "'" + precioPlato + "'" + "," + "DescripcionPlato=" + "'" + descripcionPlato + "'";
        try{
            Conector.getConector().ejecutarSQL(query);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void eliminar(double precioPlato, String descripcionPlato) {
        String query = "DELETE FROM PLATO WHERE PrecioPlato=" + "'" + precioPlato +
                "'" + "AND" + "DescripcionPlato=" + "'" + descripcionPlato + "'";
        try{
            Conector.getConector().ejecutarSQL(query);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public ArrayList<Plato> listar() {
        ArrayList<Plato> plato = new ArrayList();
        String sql = "SELECT * FROM DIRECCIONES";
        ResultSet rs;
        try {
            rs = Conector.getConector().ejecutarSQL(sql, true);
            while (rs.next()) {
                String descripcionPlato;
                double precioPlato;
                precioPlato =  rs.getDouble("PrecioPlato");
                descripcionPlato = rs.getString("DescripcionPlato");




                Plato tmpPlato = new Plato(precioPlato,descripcionPlato);


                plato.add(tmpPlato);

            }


        } catch (Exception e) {
            e.printStackTrace();
        }
        return plato;
    }
}
