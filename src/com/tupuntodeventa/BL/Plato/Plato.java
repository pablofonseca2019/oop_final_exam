package com.tupuntodeventa.BL.Plato;

import com.tupuntodeventa.BL.Producto.Producto;

public class Plato extends Producto {

    //Attributes
    private double PrecioPlato;
    private String DescripcionPlato;

    //Constructor
    public Plato(double precioPlato, String descripcionPlato) {
        PrecioPlato = precioPlato;
        DescripcionPlato = descripcionPlato;
    }

    //Getters and Setters
    public double getPrecioPlato() {
        return PrecioPlato;
    }

    public void setPrecioPlato(double precioPlato) {
        PrecioPlato = precioPlato;
    }

    public String getDescripcionPlato() {
        return DescripcionPlato;
    }

    public void setDescripcionPlato(String descripcionPlato) {
        DescripcionPlato = descripcionPlato;
    }

    //To String
    @Override
    public String toString() {
        return "Plato{" +
                "PrecioPlato=" + PrecioPlato +
                ", DescripcionPlato=" + DescripcionPlato +
                '}';
    }

    //Equals

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Plato)) return false;
        Plato plato = (Plato) o;
        return Double.compare(plato.PrecioPlato, PrecioPlato) == 0;
    }
}
