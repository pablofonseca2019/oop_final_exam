package com.tupuntodeventa.BL.Plato;
import java.util.*;

public interface IPlato {
    public void insertar(double precioPlato, String descripcionPlato);
    public void modificar (double precioPlato, String descripcionPlato);
    public void eliminar(double precioPlato, String descripcionPlato);

    public ArrayList<Plato> listar();
}
