package com.tupuntodeventa.BL.Cupon;

import java.time.*;
import java.util.*;

public class Cupon {

    //Attributes
    private String codigoCupon;
    private double porcentajeDescuentoCupon;
    private boolean isRedeemed;//ES REDIMIDO, true si ya fue redimido, false el cupon no se ha usado
    private LocalDate fechaExpiracionCupon;

    /***
     * contructor para poder almacenar los valores de cada cupon en el listado desde la base de datos
     * @param codigoCupon codigo auto generado al azar para el cupon
     * @param porcentajeDescuentoCupon//pro
     * @param isRedeemed
     * @param fechaExpiracionCupon
     */
    public Cupon(String codigoCupon, double porcentajeDescuentoCupon, boolean isRedeemed, LocalDate fechaExpiracionCupon) {
        this.codigoCupon = codigoCupon;
        this.porcentajeDescuentoCupon = porcentajeDescuentoCupon;
        this.isRedeemed = isRedeemed;
        this.fechaExpiracionCupon = fechaExpiracionCupon;
    }

    //Getters and Setters
    public LocalDate getFechaExpiracionCupon() {
        return fechaExpiracionCupon;
    }

    public void setFechaExpiracionCupon(LocalDate fechaExpiracionCupon) {
        this.fechaExpiracionCupon = fechaExpiracionCupon;
    }

    public String getCodigoCupon() {
        return codigoCupon;
    }

    public void setCodigoCupon(String codigoCupon) {
        this.codigoCupon = codigoCupon;
    }

    public boolean getIsRedeemed() {
        return isRedeemed;
    }

    public void setIsRedeemed(boolean isRedeemed) {
        this.isRedeemed = isRedeemed;
    }

    public double getPorcentajeDescuentoCupon() {
        return porcentajeDescuentoCupon;
    }

    public void setPorcentajeDescuentoCupon(double porcentajeDescuentoCupon) {
        this.porcentajeDescuentoCupon = porcentajeDescuentoCupon;
    }

    //To String Method


    @Override
    public String toString() {
        return "Cupon{" +
                "codigoCupon='" + codigoCupon + '\'' +
                ", porcentajeDescuentoCupon=" + porcentajeDescuentoCupon +
                ", estadoCupon=" + isRedeemed +
                ", fechaExpiracionCupon=" + fechaExpiracionCupon +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Cupon)) return false;
        Cupon cupon = (Cupon) o;
        return Objects.equals(this.codigoCupon, cupon.codigoCupon) &&
                this.porcentajeDescuentoCupon == cupon.porcentajeDescuentoCupon &&
                this.isRedeemed == cupon.isRedeemed &&
                Objects.equals(this.fechaExpiracionCupon, cupon.fechaExpiracionCupon) ;
    }



}
