package com.tupuntodeventa.BL.Cupon;

import accesoBD.Conector;

import java.sql.ResultSet;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Random;

public class MySQLCuponDAO implements ICupon{
    @Override
    public void registrar(double porcentajeDescuentoCupon, LocalDate fechaExpiracionCupon) {
        boolean isRedeemed =  false;
        String codigoCupon = generateCoding();
        String query = "INSERT INTO CUPON(CodigoCupon, PorcentajeDescuentoCupon, IsRedeemed, FechaExpiracionCupon)" + " VALUES(" + "'" + codigoCupon + "'" + "," + "'" +
                porcentajeDescuentoCupon + "'" + "," + "'" + isRedeemed + "'" + "," + "'" + fechaExpiracionCupon + "'" + ")";
        try{
            Conector.getConector().ejecutarSQL(query);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void modificar(String codigoCupon,double porcentajeDescuentoCupon,boolean isRedeemed, LocalDate fechaExpiracionCupon) {
        String query =
                "UPDATE CUPON SET " + "PorcentajeDescuentoCupon=" + "'" + porcentajeDescuentoCupon + "'" + "," +
                        "IsRedeemed=" + "'" + isRedeemed + "'" + "," + "FechaExpiracionCupon=" + "'" + fechaExpiracionCupon + "'" +  "WHERE CodigoCupon=" + "'" + codigoCupon +"'";
        try{
            Conector.getConector().ejecutarSQL(query);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void eliminar(String codigoCupon) {
        String query = "DELETE FROM CUPON WHERE CodigoCupon=" + "'" + codigoCupon + "'";
        try{
            Conector.getConector().ejecutarSQL(query);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     *  Este metodo se utilizara para validar el registro de cupones si su estado es true o false
     *  True si existen cupones sin redimir por lo que no se debe permitir registrar mas cupones
     *  y False si ya solo quedan cupones redimidos
     * @return devulve un boolean con el significado de true si existe algun cupon sin redimir
     * en la tabla de la base de datos y false si la tabla de la base de datos solo tiene cupones redimidos
     */
    @Override
    public boolean existen() {
        String query = "SELECT * FROM CUPON WHERE IsRedeemed = 0";
        ResultSet rs;
        ArrayList<Cupon> cupon = new ArrayList();
        boolean existe = false;
        try{
            rs = Conector.getConector().ejecutarSQL(query, true);

            while (rs.next()) {

                String codigoCupon = rs.getString("CodigoCupon");
                Double porcentajeDescuentoCupon = rs.getDouble("PorcentajeDescuentoCupon");
                Boolean isRedeemed = rs.getBoolean("IsRedeemed");
                LocalDate fechaExpiracionCupon = rs.getDate("FechaExpiracionCupon").toLocalDate();

                Cupon tmpCupon = new Cupon(codigoCupon,porcentajeDescuentoCupon,isRedeemed,fechaExpiracionCupon);

                cupon.add(tmpCupon);

            }

        }catch(Exception e){
            e.printStackTrace();
        }
        if (cupon.toString() == "[]") {
            existe = false;
        } else {
            existe = true;
        }

        return existe;
    }

    @Override
    public ArrayList<Cupon> listar() {
        ArrayList<Cupon> cupon = new ArrayList();
        String sql = "{call listarCupon()}";
        ResultSet rs;
        try {
            rs =  Conector.getConector().ejecutarSQL(sql, true);

            while (rs.next()) {
                String codigoCupon;
                double porcentajeDescuentoCupon;
                boolean isRedeemed;
                LocalDate fechaExpiracion;
                codigoCupon = rs.getString("CodigoCupon");
                porcentajeDescuentoCupon = rs.getDouble("PorcentajeDescuentoCupon");
                isRedeemed = rs.getBoolean("IsRedeemed");
                fechaExpiracion = rs.getDate("FechaExpiracion").toLocalDate();




                Cupon tmpCupon = new Cupon(codigoCupon,porcentajeDescuentoCupon,isRedeemed,fechaExpiracion);

                cupon.add(tmpCupon);

            }


        } catch (Exception e) {
            e.printStackTrace();
        }
        return cupon;
    }

    private String generateCoding(){
        byte random[] = new byte[3];
        Random randomGenerator = new Random();
        StringBuffer buffer = new StringBuffer();

        randomGenerator.nextBytes(random);

        for (int j = 0; j < 3; j++) {
            byte b1 = (byte) ((random[j] & 0xf0) >> 4);
            byte b2 = (byte) (random[j] & 0x0f);
            if (b1 < 10)
                buffer.append((char) ('0' + b1));
            else
                buffer.append((char) ('A' + (b1 - 10)));
            if (b2 < 10)
                buffer.append((char) ('0' + b2));
            else
                buffer.append((char) ('A' + (b2 - 10)));
        }
        return (buffer.toString());
    }
}
