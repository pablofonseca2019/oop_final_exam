package com.tupuntodeventa.BL.Cupon;

import java.time.LocalDate;
import java.util.ArrayList;

public interface ICupon {
    public void registrar(double porcentajeDescuentoCupon, LocalDate fechaExpiracionCupon);
    public void modificar(String codigoCupon, double porcentajeDescuentoCupon,boolean isRedeemed, LocalDate fechaExpiracionCupon);
    public void eliminar(String codigoCupon);
    public boolean existen();

    public ArrayList<Cupon>listar();
}
