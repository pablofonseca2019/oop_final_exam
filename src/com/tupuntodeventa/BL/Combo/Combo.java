package com.tupuntodeventa.BL.Combo;

import com.tupuntodeventa.BL.Plato.Plato;
import com.tupuntodeventa.BL.Producto.Producto;

import java.util.ArrayList;
import java.util.Objects;

public class Combo extends Producto {

    //Attributes
    private ArrayList<Plato> AgrupacionPlatos;
    private String NombreCombo;
    private double CostoTotalCombo;

    //Constructor
    public Combo(String nombreCombo, double costoTotalCombo) {
        NombreCombo = nombreCombo;
        CostoTotalCombo = costoTotalCombo;
    }

    //Getters and Setters
    public String getNombreCombo() {
        return NombreCombo;
    }

    public void setNombreCombo(String nombreCombo) {
        NombreCombo = nombreCombo;
    }

    public double getCostoTotalCombo() {
        return CostoTotalCombo;
    }

    public void setCostoTotalCombo(double costoTotalCombo) {
        CostoTotalCombo = costoTotalCombo;
    }

    //To String Method
    @Override
    public String toString() {
        return "Combo{" +
                "NombreCombo='" + NombreCombo + '\'' +
                ", CostoTotalCombo=" + CostoTotalCombo +
                '}';
    }

    //Equals
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Combo)) return false;
        Combo combo = (Combo) o;
        return Double.compare(combo.CostoTotalCombo, CostoTotalCombo) == 0 &&
                Objects.equals(NombreCombo, combo.NombreCombo);
    }

}
