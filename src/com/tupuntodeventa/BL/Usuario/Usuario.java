package com.tupuntodeventa.BL.Usuario;

import java.time.*;
import java.util.Objects;

public class Usuario {

    //Attributes
    protected int ID;
    protected String tipoUsuario;
    protected String userName;
    protected String claveUsuario;
    protected String correoElectronico;
    protected String nombrePilaUsuario;
    protected String apellidoUnoUsuario;
    protected String apellidoDosUsuario;
    protected LocalDate fechaNacimientoUsuario;
    protected int edadUsuario;
    protected String generoUsuario;
    protected String identificacionUsuario;
    protected String telefonoUsuario;

    //Constructor
    public Usuario(int ID,String tipoUsuario,String userName,String claveUsuario, String correoElectronico, String nombrePilaUsuario, String apellidoUnoUsuario, String apellidoDosUsuario, LocalDate fechaNacimientoUsuario, int edadUsuario, String generoUsuario, String identificacionUsuario, String telefonoUsuario) {
        this.ID = ID;
        this.tipoUsuario = tipoUsuario;
        this.userName = userName;
        this.claveUsuario = claveUsuario;
        this.correoElectronico = correoElectronico;
        this.nombrePilaUsuario = nombrePilaUsuario;
        this.apellidoUnoUsuario = apellidoUnoUsuario;
        this.apellidoDosUsuario = apellidoDosUsuario;
        this.fechaNacimientoUsuario = fechaNacimientoUsuario;
        this.edadUsuario = edadUsuario;
        this.generoUsuario = generoUsuario;
        this.identificacionUsuario = identificacionUsuario;
        this.telefonoUsuario = telefonoUsuario;
    }


    //Getters And Setters
    public String getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(String tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getClaveUsuario() {
        return claveUsuario;
    }

    public void setClaveUsuario(String claveUsuario) {
        this.claveUsuario = claveUsuario;
    }

    public String getCorreoElectronico() {
        return correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    public String getNombrePilaUsuario() {
        return nombrePilaUsuario;
    }

    public void setNombrePilaUsuario(String nombrePilaUsuario) {
        this.nombrePilaUsuario = nombrePilaUsuario;
    }

    public String getApellidoUnoUsuario() {
        return apellidoUnoUsuario;
    }

    public void setApellidoUnoUsuario(String apellidoUnoUsuario) { this.apellidoUnoUsuario = apellidoUnoUsuario; }

    public String getApellidoDosUsuario() {
        return apellidoDosUsuario;
    }

    public void setApellidoDosUsuario(String apellidoDosUsuario) {
        this.apellidoDosUsuario = apellidoDosUsuario;
    }

    public LocalDate getFechaNacimientoUsuario() {
        return fechaNacimientoUsuario;
    }

    public void setFechaNacimientoUsuario(LocalDate fechaNacimientoUsuario) {
        this.fechaNacimientoUsuario = fechaNacimientoUsuario;
    }

    public String getGeneroUsuario() {
        return generoUsuario;
    }

    public void setGeneroUsuario(String generoUsuario) { this.generoUsuario = generoUsuario; }

    public String getIdentificacionUsuario() {
        return identificacionUsuario;
    }

    public void setIdentificacionUsuario(String identificacionUsuario) {
        this.identificacionUsuario = identificacionUsuario;
    }

    public String getTelefonoUsuario() {
        return telefonoUsuario;
    }

    public void setTelefonoUsuario(String telefonoUsuario) {
        this.telefonoUsuario = telefonoUsuario;
    }

    public int getEdadUsuario() {
        return edadUsuario;
    }

    public void setEdadUsuario(int edadUsuario) {
        this.edadUsuario = edadUsuario;
    }

    //To String Method


    @Override
    public String toString() {
        return "Usuario{" +
                "ID=" + ID +
                ", tipoUsuario='" + tipoUsuario + '\'' +
                ", userName='" + userName + '\'' +
                ", claveUsuario='" + claveUsuario + '\'' +
                ", correoElectronico='" + correoElectronico + '\'' +
                ", nombrePilaUsuario='" + nombrePilaUsuario + '\'' +
                ", apellidoUnoUsuario='" + apellidoUnoUsuario + '\'' +
                ", apellidoDosUsuario='" + apellidoDosUsuario + '\'' +
                ", fechaNacimientoUsuario=" + fechaNacimientoUsuario +
                ", edadUsuario=" + edadUsuario +
                ", generoUsuario='" + generoUsuario + '\'' +
                ", identificacionUsuario='" + identificacionUsuario + '\'' +
                ", telefonoUsuario='" + telefonoUsuario + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Usuario)) return false;
        Usuario usuario = (Usuario) o;
        if (!getUserName().equals(usuario.getUserName())) return false;
        if (!getCorreoElectronico().equals(usuario.getCorreoElectronico())) return false;
        return getIdentificacionUsuario().equals(usuario.getIdentificacionUsuario());

    }



}
