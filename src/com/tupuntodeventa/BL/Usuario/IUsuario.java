package com.tupuntodeventa.BL.Usuario;

import java.time.LocalDate;
import java.util.ArrayList;

public interface IUsuario {
    public void insertar(String userName,String claveUsuario, String correoElectronico, String nombrePilaUsuario,
                         String apellidoUnoUsuario, String apellidoDosUsuario, LocalDate fechaNacimientoUsuario,
                         int edadUsuario, String generoUsuario, String identificacionUsuario, String telefonoUsuario);
    public void modificar (String claveUsuario, String correoElectronico, String nombrePilaUsuario,
                           String apellidoUnoUsuario, String apellidoDosUsuario, LocalDate fechaNacimientoUsuario,
                           int edadUsuario, String generoUsuario, String identificacionUsuario, String telefonoUsuario);
    public void eliminar(String userName, String claveUsuario);
    public ArrayList<Usuario> listar();
}
