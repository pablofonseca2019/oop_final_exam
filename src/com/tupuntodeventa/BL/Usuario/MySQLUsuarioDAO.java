package com.tupuntodeventa.BL.Usuario;

import accesoBD.*;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.util.ArrayList;

public class MySQLUsuarioDAO implements IUsuario {
    @Override
    public void insertar(String userName,String claveUsuario, String correoElectronico, String nombrePilaUsuario, String apellidoUnoUsuario,
                         String apellidoDosUsuario, LocalDate fechaNacimientoUsuario, int edadUsuario, String generoUsuario, String identificacionUsuario, String telefonoUsuario) {

    }

    @Override
    public void modificar(String claveUsuario, String correoElectronico, String nombrePilaUsuario, String apellidoUnoUsuario, String apellidoDosUsuario, LocalDate fechaNacimientoUsuario, int edadUsuario, String generoUsuario, String identificacionUsuario, String telefonoUsuario) {
        String query =
                "UPDATE USUARIOS SET  " + "ClaveUsuario=" + "'" + claveUsuario + "'" + "," + "CorreoElectronico=" + "'" + correoElectronico + "'" + "," +
                        "NombrePilaUsuario=" + "'" + nombrePilaUsuario + "'" + "," + "ApellidoUnoUsuario=" + "'" + apellidoUnoUsuario + "'" + "," + "ApellidoDosUsuario=" +
                        "'" + apellidoDosUsuario + "'" + "," + "FechaNacimientoUsuario=" + "'" + fechaNacimientoUsuario + "'" + "," + "EdadUsuario=" + "'" + edadUsuario +
                        "'" + "," + "GeneroUsuario=" + "'" + generoUsuario + "'" + "," + "identificacionUsuario=" + "'" + identificacionUsuario +  "'"
                        + "," +"TelefonoUsuario=" + "'" + telefonoUsuario + "'" + "WHERE ClaveUsuario=" + "'" + claveUsuario + "'" + "AND" + "CorreoElectronico=" + "'" + correoElectronico + "'";
        try{
            Conector.getConector().ejecutarSQL(query);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void eliminar(String userName, String claveUsuario) {
        String query = "DELETE FROM USUARIOS WHERE UserName=" + "'" + userName + "'" + "AND" + "ClaveUsuario=" + "'" + claveUsuario + "'";
        try{
            Conector.getConector().ejecutarSQL(query);
        }catch(Exception e){
            e.printStackTrace();
        }

    }
    @Override
    public ArrayList<Usuario> listar() {
        ArrayList<Usuario> usuarios = new ArrayList();
        String sql = "SELECT * FROM USUARIOS";
        ResultSet rs;
        try {
            rs = Conector.getConector().ejecutarSQL(sql,true);

            while (rs.next()) {
                String tipoUsuario, userName,claveUsuario,correoElectronico,nombrePilaUsuario,
                apellidoUnoUsuario,apellidoDosUsuario,generoUsuario,identificacionUsuario,telefonoUsuario;
                int ID,edadUsuario;
                LocalDate fechaNacimientoUsuario;
                ID = rs.getInt("claveUsuario");
                tipoUsuario = rs.getString("claveUsuario");
                userName = rs.getString("claveUsuario");
                claveUsuario = rs.getString("claveUsuario");
                correoElectronico = rs.getString("correoElectronico");
                nombrePilaUsuario = rs.getString("nombrePilaUsuario");
                apellidoUnoUsuario = rs.getString("apellidoUnoUsuario");
                apellidoDosUsuario = rs.getString("apellidoDosUsuario");
                fechaNacimientoUsuario = rs.getDate("fechaNacimientoUsuario").toLocalDate();
                edadUsuario =  rs.getInt("edadUsuario");
                generoUsuario = rs.getString("generoUsuario");
                identificacionUsuario = rs.getString("identificacionUsuario");
                telefonoUsuario = rs.getString("nombrePilaUsuario");




                Usuario tmpUsuario = new Usuario (ID,tipoUsuario,userName,claveUsuario,correoElectronico,nombrePilaUsuario,apellidoUnoUsuario,apellidoDosUsuario,fechaNacimientoUsuario,edadUsuario,generoUsuario,identificacionUsuario,telefonoUsuario);


                usuarios.add(tmpUsuario);

            }


        } catch (Exception e) {
            e.printStackTrace();
        }
        return usuarios;
    }
}
